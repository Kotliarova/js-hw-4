// Описати своїми словами навіщо потрібні функції у програмуванні.
// Функції потрібні для того щоб одну і ту же дію ми могли один раз прописати (в функції), та потім її викликати будь-де у коді не пописуючи її знову повністю.

// Описати своїми словами, навіщо у функцію передавати аргумент.
// Функція по факту і працює з аргументами. Тобто у функції прописується що потрібно зробити з агрументами та що повернути. Аргументи можуть мати різний тип та значення а також їх може бути багато. Коли ми викликаємо функцію ми в дужках прописуємо аргументи з якими функція і буде працювати, тобто при кожному новому виклику функції прописується значення аргументів.

// Що таке оператор return та як він працює всередині функції?
// Оператор return повертає значення. В тілі функції ми можемо будь-де прописати return, що буде означати що функція припиняється та повертає нам значення (результат) який ми вказали в return або ми можемо не вказувати значення (результат) в return тоді ми негайно вийдемо з функції.


let number1 = prompt('Enter first number');
let number2 = prompt('Enter second number');
let sign = prompt('Choose and enter math operation: +, -, *, /');

while ( !number1 || isNaN(number1) || !number2 || isNaN(number2)) {
    number1 = prompt('Enter first number', [number1]);
    number2 = prompt('Enter second number', [number2]);  
}

function doMathOperation (a, b, c) {
    switch (sign) {
        case "+": 
            return Number(a) + Number(b);
        case "-": 
            return a - b;
        case "*": 
            return a * b;
        case "/": 
            return a / b;
    }
}

let result = doMathOperation (number1, number2, sign);
console.log(result);